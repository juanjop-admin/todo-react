import React, { useState, useEffect } from 'react';
import { TaskRow } from './components/TaskRow';
import { TaskBanner } from './components/TaskBanner';
import { TaskCreator } from './components/TaskCreator';
import { VisibilityControl } from './components/VisibilityControl';

import Table from 'react-bootstrap/Table';

function App() {
	const [userName, setUserName] = useState('Juan José');
	const [taskItems, setTaskItems] = useState([
		{ name: 'Task One', done: false },
		{ name: 'Task Two', done: false },
		{ name: 'Task Three', done: true },
		{ name: 'Task Four', done: false },
	]);

	const [showCompleted, setShowCompleted] = useState(true);

	useEffect(() => {
		let data = localStorage.getItem('tasks');
		if (data != null) {
			setTaskItems(JSON.parse(data));
		} else {
			setUserName('Juan example');
			setTaskItems([
				{ name: 'Task One example', done: false },
				{ name: 'Task Two example', done: false },
				{ name: 'Task Three example', done: true },
				{ name: 'Task Four example', done: false },
			]);
			setShowCompleted(true);
		}
	}, []);

	useEffect(() => {
		localStorage.setItem('tasks', JSON.stringify(taskItems));
	}, [taskItems]);

	const createNewTask = (taskName) => {
		if (!taskItems.find((t) => t.name === taskName)) {
			setTaskItems([...taskItems, { name: taskName, done: false }]);
		}
	};

	const toggleTask = (task) =>
		setTaskItems(
			taskItems.map((t) =>
				t.name === task.name ? { ...t, done: !t.done } : t,
			),
		);

	const taskTableRows = (doneValue) =>
		taskItems
			.filter((task) => task.done === doneValue)
			.map((task) => (
				<TaskRow task={task} key={task.name} toggleTask={toggleTask} />
			));

	return (
		<>
			<TaskBanner userName={userName} taskItems={taskItems} />
			<TaskCreator callback={createNewTask} />
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>Description</th>
						<th>Done</th>
					</tr>
				</thead>
				<tbody>{taskTableRows(false)}</tbody>
			</Table>

			{showCompleted && (
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>Description</th>
							<th>Done</th>
						</tr>
					</thead>
					<tbody>{taskTableRows(true)}</tbody>
				</Table>
			)}

			<div className="bg-secondary-text-white text-center p-2">
				<VisibilityControl
					description="Completed Task"
					isChecked={showCompleted}
					callback={(checked) => setShowCompleted(checked)}
				/>
			</div>
		</>
	);
}

export default App;
