import React, { useState } from 'react';

import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

export const TaskCreator = (props) => {
	const [newTaskName, setNewTaskName] = useState('');

	const updateNewTaskValue = (e) => setNewTaskName(e.target.value);

	const createNewTask = () => {
		props.callback(newTaskName);
		setNewTaskName('');
	};

	return (
		<Card>
			<Card.Body>
				<div className="my-1">
					<input
						type="text"
						className="form-control"
						value={newTaskName}
						onChange={updateNewTaskValue}
					/>
					<br />
					<Button variant="primary" size="lg" block onClick={createNewTask}>
						Add
					</Button>
					{''}
				</div>
			</Card.Body>
		</Card>
	);
};
